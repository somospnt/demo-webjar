package com.somospnt.demowebjar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoWebjarApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoWebjarApplication.class, args);
    }
}
